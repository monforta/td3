/**
 *
 * @param a
 * @param b
 * @return le max entre a et b
 */
int max2(int a, int b ) {

  if(a>b){
      return a;
  }
  return b;

}

/**
 *
 * @param a
 * @param b
 * @param c
 * @return le max entre a, b et c
 */
int max3(int a, int b , int c) {
    if (max2(a,b)== b && max2(b,c)==b){
        return b;
    }
    else if (max2(a,b)==a && max2(b,c)==b){
        return b;
    }
    else if (max2(a,b)==a && max2(b,c)==c){
        return b;
    }
    else {
        return c;
    }
}

void testMax() {
    Ut.afficher("combien de nombre voulez-vous comparer");
    int n = Ut.saisirEntier();
    if (n==2){
        Ut.afficher("veuillez entrer un nombre ");
        int a = Ut.saisirEntier();
        Ut.afficher("veuillez entrer un nombre ");
        int b = Ut.saisirEntier();
        Ut.afficher("le plus grand nombre est ");
        Ut.afficher(max2(a,b));
    }
    else if (n==3){
        Ut.afficher("veuillez entrer un nombre ");
        int a = Ut.saisirEntier();
        Ut.afficher("veuillez entrer un nombre ");
        int b = Ut.saisirEntier();
        Ut.afficher("veuillez entrer un nombre ");
        int c = Ut.saisirEntier();
        Ut.afficher("le plus grand nombre est ");
        Ut.afficher(max3(a,b,c));
    }


}
/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture, en base 10, de n
 */
int nbChiffres(int n){
    int nbchiffre = 1;
    while (n/10 !=0){
        nbchiffre = nbchiffre +1;
        n=n/10;
    }
    return nbchiffre;
}

/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture du nombre n^2
 */

int nbChiffresDuCarre(int n){
    return nbChiffres(n*n);
}

/**
 * afficher nb caractères car (à partir de la position courante du curseur sur le terminal).
 * @param nb
 * @param car
 */
void  repetCarac(int nb, char car){
    for (int i=0; i < nb-1; i++) {
        Ut.afficher(car);
    }
}

/**
 * affiche à l'écran une pyramide de hauteur h constituée de lignes répétant le caractère c.
 * @param h
 * @param c
 */
void pyramideSimple (int h  , char  c ){
    for(int i=1; i < h;  i++) {
        for (int j=h ; j > i; j--){
            Ut.afficher(' ');
        }
        repetCarac(i*2,c);
        Ut.afficherSL("");

    }

}

void testPyramideSimple (){
    Ut.afficher("veuillez rentrer la hauteur de votre pyramide");
    int h = Ut.saisirEntier();
    Ut.afficher("veuillez rentrer le caractère voulu pour la pyramide");
    char c = Ut.saisirCaractere();
    pyramideSimple(h,c);
}
/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre croissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresCroissants (int nb1,int nb2 ){
    if (nb1<=nb2){
        for (int i=0; i<=nb2-nb1; i++){
            Ut.afficher((nb1+i)%10);
        }
    }
}

/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre décroissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresDecroissants (int nb1,int nb2 ){
    if(nb1<=nb2){
        for(int j=nb2-nb1; j>=0; j--){
            Ut.afficher((nb1+j)%10);
        }
    }

}

/**
 * permet de représenter à l'écran la pyramide
 * @param h
 */

void pyramide(int h ) {
    int nbespace = h-1;
    for (int i=0; i <= h; i++) {
        repetCarac(nbespace-i, ' ');
        afficheNombresCroissants(i, 2*i-1);
        afficheNombresDecroissants(i,2*i-2);
        Ut.afficherSL(' ');

    }

}


/**
 * @param c
 * @return la racine carrée entière n d'un nombre entier c donné,
 * si c est un carré parfait, c'est-à-dire si c = n * n.
 * Sinon la fonction retourne -1.
 */
int racineParfaite(int c) {
    int n =0;
    for (int i=1;i<=c;i++) {
        if (i * i == c) {
            n = i;
        }
    }
    if (n==0){
        return -1;
    }
    else { //c est un carré parfait
        return n;
    }
}
/**
 *
 * @param nb
 * @return vrai si un entier donné est un carré parfait, faux sinon.
 */
boolean estCarreParfait(int nb){
    if (racineParfaite(nb)!=-1) {
        return true;
    }
    else {
        return false;
    }
}


/**
 * @param p
 * @param q
 * @return vrai  si deux nombres entiers `p`et `q` sont amis, faux sinon.
 */

boolean nombresAmicaux(int p , int q){
    int sommediviseursp = 0;
    int sommediviseursq = 0;
    for (int i=1; i<=p; i++){
        if (i%p==0 && p!=i) {
            sommediviseursp = sommediviseursp+ i;
        }
    }

    for (int i=1; i<=q; i++){
        if (i%q==0 && i!=q) {
            sommediviseursq = sommediviseursq + i;
        }
    }

    return sommediviseursq==p+q && sommediviseursp==q+p;



}

/**
 * affiche à l'écran les couples de
 *nombres amis parmi les nombres inférieurs ou égaux à max.
 * @param max
 */

void afficheAmicaux(int max){
    for (int i=1; i<=max;i++) {
        for (int j = i+1; j <= max; j++) {
            if (nombresAmicaux(i,j)){
                Ut.afficher(i + " et " + j);
            }
        }
    }
}
//exercice en plus qui était sur l'évaluation
int sommediviseursnonT(int n){
    int sommediviseurs = 0;
    for (int i=2; i<=n; i++) {
        if(i%n==0 && i!=n){
            sommediviseurs = sommediviseurs + i;
        }
    }
    return sommediviseurs;
}
/**
 *
 * @param c1
 * @param c2
 * @return vrai si deux entiers donnés peuvent  être les côtés de l'angle droit d'un triangle rectangle dont les 3
 *        côtés sont des nombres entiers, faux sinon
 */
boolean  estTriangleRectangle (int c1, int c2){
    if (estCarreParfait(c1*c1+c2*c2)==true){
        return true;
    }
    else {
        return false;
    }
}

int nbsyracusien(int n){
    int nbsyracus=n;
    while(nbsyracus!=1){
        if(nbsyracus%2==0){
            nbsyracus = nbsyracus/2;
        }
        else if (nbsyracus%2==1){
            nbsyracus = 3*nbsyracus+1;
        }
    }
    return n;
}

boolean NBsyracusiens(int n, int nbMaxOp){
    for (int i=0;i<=nbMaxOp;i++){
        if(n%2==0) {
            n = n / 2;
        }
        else if (n%2==1){
            n = 3*n+1;
        }
    }
    if (n==1){
        return true;
    }
    else{
        return false;
    }
}

void main (){
Ut.afficher("");
Ut.afficher(sommediviseursnonT(40));

}
